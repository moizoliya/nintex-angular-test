import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';

import { FlightSearchComponent } from './search.component';
 
import { FlightSearchService } from './search.service';


@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      { path: 'flights', component: FlightSearchComponent }

    ])
  ],
  declarations: [
    FlightSearchComponent
  ],
  providers: [
   FlightSearchService
  ]
})
export class FlightModule { }
