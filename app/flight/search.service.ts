import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import { FlightSearchResultItem , FlightSearchOption} from './model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import * as _ from 'underscore';

@Injectable()
export class FlightSearchService {
    private baseUrl = 'http://nmflightapi.azurewebsites.net/api/flight';

    constructor(private http: Http) { }

    search(searchOption:FlightSearchOption): Observable<FlightSearchResultItem[]> {

        let params = new URLSearchParams();
        params.set('DepartureAirportCode', searchOption.from);
        params.set('ArrivalAirportCode', searchOption.to);
        params.set('DepartureDate', searchOption.departOn);
        params.set('ReturnDate', searchOption.returnOn);
        const url = `${this.baseUrl}`;
        
        return this.http.get(url, { search: params })
            .map(response => this.extractData(response))
            .catch(this.handleError);
    }


    private extractData(response: Response) {
        let body = response.json();
        let result:any;
        if(_.isArray(body)){
            result = [];
            _.each(body, item => result.push(this.transformResult(item)));
        }
        else{
            result = body;
        }

        return result;
    }

    private transformResult(dataItem:any):any {
        if(undefined===dataItem)
            return;
       
        let result = {};
        let keys=  Object.keys(dataItem);
        for (var key of keys) {
            let newKey = key.substring(0,1).toLowerCase() + key.substring(1);
            result[newKey] = dataItem[key];    
        }
        return result;
    }

    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }


}