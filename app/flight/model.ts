/* Defines the product entity */
export interface FlightSearchResultItem {
    airlineLogoAddress: string;
    airlineName: string;
    inboundFlightsDuration: string;
    itineraryId: string;
    outboundFlightsDuration: string;
    stops: number;
    totalAmount: number;
}


export interface FlightSearchOption{
    from:string;
    to:string;
    departOn:string;
    returnOn:string;

}