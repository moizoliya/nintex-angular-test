import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ValidationService } from '../shared/validation.service';
import { NotificationService } from '../shared/notification.service';


import { FlightSearchService } from './search.service';

import { FlightSearchOption, FlightSearchResultItem } from './model';
import * as _ from 'underscore';
@Component({
    moduleId:module.id.toString(),
    selector: 'flight-search',
    templateUrl: 'search.component.html'
})
export class FlightSearchComponent implements OnInit {
    frm: FormGroup;
    pageTitle : string;
    flights:FlightSearchResultItem[];
    errorMessage: string;


    // Form control
    ctrlFrom : AbstractControl;
    ctrlTo : AbstractControl;
    ctrlDepartOn : AbstractControl;
    ctrlReturnOn : AbstractControl;

    constructor(private formBuilder: FormBuilder,
                 private activatedRoute:ActivatedRoute,
                 private router:Router,
                 private flightSrchService:FlightSearchService ,
                 private notificationService:NotificationService) {
  
    }

    ngOnInit(){
        this.pageTitle = 'Search Flights';

         // individual form components
        this.ctrlFrom = this.formBuilder.control('',Validators.required);
        this.ctrlTo = this.formBuilder.control('',Validators.required);
        this.ctrlDepartOn = this.formBuilder.control('', [Validators.required, ValidationService.dateValidator]);
        this.ctrlReturnOn = this.formBuilder.control('', [Validators.required, ValidationService.dateValidator]);

        // create Form
        this.frm = this.formBuilder.group({
            'from': this.ctrlFrom,
            'to': this.ctrlTo,
            'travelPeriod' : this.formBuilder.group({
                'departOn': this.ctrlDepartOn,
                'returnOn': this.ctrlReturnOn,
            }, {validator : ValidationService.travelDateRange})
            
        });
 
        // set value from query string
        this.activatedRoute.queryParams.subscribe(params => {
                this.setControlValueFromParam(this.ctrlDepartOn, params, 'departOn');
                this.setControlValueFromParam(this.ctrlReturnOn, params, 'returnOn');
                this.setControlValueFromParam(this.ctrlTo, params, 'to');
                this.setControlValueFromParam(this.ctrlFrom, params, 'from');
                this.search();
        });
    }

    
   
    search() {
        if (this.frm.valid) {
            let dlg = this.notificationService.showLoading();
            this.clearSearchResult();
            this.flightSrchService.search(this.makeSearchOption())
                            .subscribe(data => {  this.setFlightResult(data); dlg.destroy(); }, 
                            error => { this.setSearchError(error); dlg.destroy(); });
        }                     
    }


    clearSearchResult(){
            this.errorMessage=null;
            this.flights=[];
    }

    setSearchError(error:any){
        this.errorMessage = error;
        this.flights= [];
       
    }

    setFlightResult(result:FlightSearchResultItem[]){
        this.flights= result;
        this.errorMessage = null;
    }

    navigateToSearch(){
        if (this.frm.dirty && this.frm.valid) {
            this.router.navigate(['/flights'], { queryParams : this.makeSearchOption()});
        }
    }

    private setControlValueFromParam(ctrl:AbstractControl, params:{[key:string]:any}, key:string, defaultValue:any=''){
        var value = params[key] ? params[key] : defaultValue;
        ctrl.setValue(value);
    }

    private makeSearchOption() : FlightSearchOption{
        let result : FlightSearchOption = {
            departOn : this.ctrlDepartOn.value,
            returnOn : this.ctrlReturnOn.value,
            from : this.ctrlFrom.value,
            to: this.ctrlTo.value
        };
        return result;
    }
    
}
