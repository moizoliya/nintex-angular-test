import { Component , ViewContainerRef } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError, NavigationCancel } from '@angular/router';
import {NotificationService } from './shared/notification.service'
 
@Component({
    selector: 'pm-app',
    templateUrl: './app/app.component.html'
})
export class AppComponent {
    pageTitle: string = 'Nintex Flight Search';
    loading: boolean = true;

    constructor(
                private viewContainerRef: ViewContainerRef,
                private router: Router,
                private notificationService : NotificationService
                ) {

        notificationService.viewContainerRef = viewContainerRef;                    
        router.events.subscribe((routerEvent: Event) => {
            this.checkRouterEvent(routerEvent);
        });
    }

    checkRouterEvent(routerEvent: Event): void {
        if (routerEvent instanceof NavigationStart) {
            this.loading = true;
        }

        if (routerEvent instanceof NavigationEnd ||
            routerEvent instanceof NavigationCancel ||
            routerEvent instanceof NavigationError) {
            this.loading = false;
        }
    }
 
}
