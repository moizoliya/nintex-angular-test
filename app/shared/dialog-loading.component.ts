import { Component} from '@angular/core';
@Component({
    selector: 'dialog-loading',
    moduleId:module.id,
    styleUrls:['dialog-loading.css'],
    template: '<div class="dialog-loading"><div>Loading...<img src="http://www.socialups.com/static/images/fbinventory/ajax_loader.gif"></div></div>',
})
export class DialogLoadingComponent {

}