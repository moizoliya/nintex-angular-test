import { NgModule }  from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';


import { ValidationService } from './validation.service';
import { NotificationService } from './notification.service';

import { ControlMessages } from './control-messages.component';

 
import { DialogLoadingComponent } from './dialog-loading.component';


@NgModule({
  imports: [ CommonModule],
  exports : [
    CommonModule,
    ReactiveFormsModule,
    
    ControlMessages,
    DialogLoadingComponent
  ],
  entryComponents : [DialogLoadingComponent],
  declarations: [  ControlMessages, DialogLoadingComponent ],
  providers : [ValidationService , NotificationService]
})
export class SharedModule { }
