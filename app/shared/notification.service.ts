import { Injectable, ViewContainerRef, ReflectiveInjector, ComponentFactoryResolver,ComponentRef  } from '@angular/core';
import {DialogLoadingComponent}  from './dialog-loading.component'
import {DialogType}  from './model'

@Injectable()
export class NotificationService {
      viewContainerRef: ViewContainerRef;

    constructor(private resolver: ComponentFactoryResolver) {
    }

      private  showDialog(dialogType:DialogType, dialogComponent:any=null, inputs:any =null):ComponentRef<any>{
        let factory:any=null;
            switch(dialogType){
            case DialogType.Custom:
                factory = this.resolver.resolveComponentFactory(dialogComponent);
                break;
            case DialogType.Loading:
                factory = this.resolver.resolveComponentFactory(DialogLoadingComponent);
                break;

            default:
                break;    
            }
        
            if(inputs)
            {
                // Inputs need to be in the following format to be resolved properly
                let inputProviders = Object.keys(inputs).map((inputName) => {return {provide: inputName, useValue: inputs[inputName]};});
                let resolvedInputs = ReflectiveInjector.resolve(inputProviders);
                
                // We create an injector out of the data we want to pass down and this components injector
                let injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs,
                                this.viewContainerRef.injector);
            }
            
        
        let component = this.viewContainerRef.createComponent(
                                factory,
                                this.viewContainerRef.length,
                                this.viewContainerRef.injector);
        return component;
    }


 

     showLoading():ComponentRef<any>{
         return this.showDialog(DialogType.Loading);
     }
    
}